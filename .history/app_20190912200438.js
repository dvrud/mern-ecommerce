const express = require("express");
require("dotenv").config();
const app = express();

app.get("/", async (req, res) => {
  res.send("Node ecommerce");
});

const port = process.env.PORT || 8001;

app.listen(port, () => {
  console.log(`Server Started on port ${port}`);
});
