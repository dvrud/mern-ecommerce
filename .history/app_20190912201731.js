const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");
const app = express();

// Database connection
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log(`Database connected on ${process.env.MONGO_URI}`);
  });

app.get("/", async (req, res) => {
  res.send("Node ecommerce");
});

const port = process.env.PORT || 8001;

app.listen(port, () => {
  console.log(`Server Started on port ${port}`);
});
