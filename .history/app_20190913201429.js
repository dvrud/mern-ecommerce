const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const app = express();

// Database connection
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log(`Database connected on ${process.env.MONGO_URI}`);
  });

//   MOrgan middleware
// app.use(morgan("dev"));

// Bodyparser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Cookie parser middleware
app.use(cookieParser());

bodyParser.json();
//   Importing the routes
app.use("/api", require("./routes/auth"));
const port = process.env.PORT || 8001;

app.listen(port, () => {
  console.log(`Server Started on port ${port}`);
});
