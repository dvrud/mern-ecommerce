const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");
const { createProduct } = require("../controllers/product");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const router = express.Router();

// Cloudinary config
cloudinary.config({
  cloud_name: "dlymhxrrs",
  api_key: "178736849279487",
  api_secret: "zeOYujTELAaGSqhXjcRCQX3GSYU"
});

const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  folder: "Cloudinary Ecommerce",
  allowedFormats: ["jpg", "png"],
  transformation: [{ width: 500, height: 500, crop: "limit" }]
});

const parser = multer({ storage: storage });

router.post(
  "/createProduct",
  [
    check("name", "Name is Requried")
      .not()
      .isEmpty(),
    check("quantity", "Please include product quantity")
      .not()
      .isEmpty(),
    check("price", "Please enter a price for product")
      .not()
      .isEmpty(),
    auth,
    parser.array("image", 5)
  ],
  createProduct
);
module.exports = router;
