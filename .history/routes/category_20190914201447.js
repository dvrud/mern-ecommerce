const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");
const { createCategory, allCategories } = require("../controllers/category");
const router = express.Router();

router.post("/createCategory", auth, createCategory);

router.get("/allCategories", auth, allCategories);
module.exports = router;
