const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");

const { signup, signin, signout, updateUser } = require("../controllers/auth");
const router = express.Router();

router.post(
  "/registerUser",
  [
    check("name", "Name is Requried")
      .not()
      .isEmpty(),
    check("email", "Please include a valid Email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  signup
);

router.post(
  "/userLogin",
  [
    check("email", "Please include a valid Email").isEmail(),
    check("password", "Password is required").exists()
  ],
  signin
);

router.get("/userSignout", signout);

router.get("/api/authUser", auth, async (req, res) => {
  console.log("load user");
  try {
    const user = await User.findById(req.user).select("-password");
    response.sendResponse(res, user, 200);
  } catch (err) {
    console.error(err.message);
    response.sendResponse(res, err.message, 400);
  }
});

router.post("/updateUser", auth, updateUser);
module.exports = router;
