const express = require("express");
const { sayHi } = require("../controllers/user");
const router = express.Router();

router.get("/", async (req, res) => {
  sayHi(req, res);
});

module.exports = router;
