const express = require("express");
const { check, validationResult } = require("express-validator");

const { signup, signin, signout } = require("../controllers/user");
const router = express.Router();

router.post(
  "/registerUser",
  [
    check("name", "Name is Requried")
      .not()
      .isEmpty(),
    check("email", "Please include a valid Email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  signup
);

router.post(
  "/userLogin",
  [
    check("email", "Please include a valid Email").isEmail(),
    check("password", "Password is required").exists()
  ],
  signin
);

router.get("/userSignout", signout);
module.exports = router;
