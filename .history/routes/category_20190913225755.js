const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");
const { createCategory } = require("../controllers/category");
const router = express.Router();

router.get("/dm", (req, res) => {
  res.send({ data: "data" });
});
router.post("/createCategory", auth);

module.exports = router;
