const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");
const { createProduct } = require("../controllers/product");
const router = express.Router();

router.post("/createProduct", auth, createProduct);
module.exports = router;
