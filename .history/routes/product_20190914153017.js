const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");
const {
  createProduct,
  singleProduct,
  deleteProduct
} = require("../controllers/product");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const router = express.Router();

// Cloudinary config
cloudinary.config({
  cloud_name: "dlymhxrrs",
  api_key: "178736849279487",
  api_secret: "zeOYujTELAaGSqhXjcRCQX3GSYU"
});

const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  folder: "Cloudinary Ecommerce",
  allowedFormats: ["jpg", "png"],
  transformation: [{ width: 500, height: 500, crop: "limit" }]
});

const parser = multer({ storage: storage });

router.post(
  "/createProduct",
  [
    auth,
    parser.array("image", 5),
    [
      check("name", "Please include product name")
        .not()
        .isEmpty(),
      check("quantity", "Please include product quantity")
        .not()
        .isEmpty(),
      check("price", "Please include product Price")
        .not()
        .isEmpty()
    ]
  ],
  createProduct
);

router.get("/singleProduct", auth, singleProduct);

router.delete("/deleteProduct", auth, deleteProduct);

router.post("/image", async (req, res) => {
  cloudinary.uploader.destroy("").then(data => {
    console.log(data);
  });
});
module.exports = router;
