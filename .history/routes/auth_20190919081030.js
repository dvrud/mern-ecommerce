const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");

const { signup, signin, signout, updateUser } = require("../controllers/auth");
const router = express.Router();
const User = require("../models/User");
const response = require("../HelperFunctions/response");

router.post(
  "/registerUser",
  [
    check("name", "Name is Requried")
      .not()
      .isEmpty(),
    check("email", "Please include a valid Email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  signup
);

router.post(
  "/userLogin",
  [
    check("email", "Please include a valid Email").isEmail(),
    check("password", "Password is required").exists()
  ],
  signin
);

router.get("/userSignout", signout);

router.get("/authUser", auth, async (req, res) => {
  console.log(req.user, "this is auth user");
  try {
    const user = await User.findById(req.user._id).select("-password");
    console.log(user);
    response.sendResponse(res, user, 1);
  } catch (err) {
    console.error(err.message);
    response.sendResponse(res, err.message, 0);
  }
});

router.post("/updateUser", auth, updateUser);
module.exports = router;
