const express = require("express");
const { sayHi } = require("../controllers/user");
const router = express.Router();

router.get("/", async (req, res) => {
  res.send("Node ecommerce");
});

module.exports = router;
