const jwt = require("jsonwebtoken");
const credentials = require("../credentials").credentials;
const response = require("../HelperFunctions/response");
const verifyToken = (req, res, next) => {
  // const token = req.headers["x-auth-token"];
  // or
  const token = req.header("access-token");
  if (!token) {
    return response.sendResponse(res, "No Token Provided", 400);
  }

  try {
    const decoded = jwt.verify(token, credentials.jwtSecret);
    if (!decoded) {
      return response.sendResponse(res, "Token is not valid", 400);
    }
    req.user = decoded;
    next();
  } catch (err) {
    return response.sendResponse(res, "Token is not valid", 400);
  }
};

module.exports = verifyToken;
