const sendResponse = (res, data, status) => {
  res.send({ success: status, data: data });
};

module.exports = {
  sendResponse
};
