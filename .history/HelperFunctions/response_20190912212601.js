const sendResponse = (res, data, status) => {
  res.status(status).json({ data: data });
};

module.exports = {
  sendResponse
};
