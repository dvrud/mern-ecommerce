const sendResponse = (res, data, status) => {
  // res.status(status).json({ data: data });
  res.send({ status: status, data: data });
};

module.exports = {
  sendResponse
};
