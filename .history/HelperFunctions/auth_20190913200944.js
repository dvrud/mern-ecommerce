const jwt = require("jsonwebtoken");
const credentials = require("../credentials").credentials;
const response = require("./sendResponse");
const verifyToken = (req, res, next) => {
  // const token = req.headers["x-auth-token"];
  // or
  const token = req.header("access-token");
  if (!token) {
    return response.sendResponse(res, "No Token Provided", 0);
  }

  try {
    const decoded = jwt.verify(token, credentials.jwtSecret);
    if (!decoded) {
      return response.sendResponse(res, "Token is not valid", 0);
    }
    req.user = decoded.id;
    next();
  } catch (err) {
    return response.sendResponse(res, "Token is not valid", 0);
  }
};

module.exports = verifyToken;
