const sendResponse = (res, status, data) => {
  res.send({ success: status, data: data });
};

module.exports = {
  sendResponse
};
