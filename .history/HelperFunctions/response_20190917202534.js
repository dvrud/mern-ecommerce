const sendResponse = (res, data, status) => {
  // res.status(status).json({ data: data });
  res.send({ success: status, data: data });
};

module.exports = {
  sendResponse
};
