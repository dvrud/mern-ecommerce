const User = require("../models/User");
const bcrypt = require("bcryptjs");
const { check, validationResult } = require("express-validator");
const response = require("../HelperFunctions/response");
const jwt = require("jsonwebtoken");
// Credentails
const credentails = require("../credentials").credentials;

exports.signup = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.errors[0].msg);
    return response.sendResponse(res, errors.errors[0].msg, 400);
  }
  const { name, password, email, about } = req.body;
  const newUser = {
    name,
    password,
    email,
    about
  };
  const users = await User.findOne({ email });
  if (users) {
    return response.sendResponse(res, "User Already Exist", 400);
  }

  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(password, salt);

  const user = await new User(newUser).save().catch(err => {
    response.sendResponse(res, err, 400);
  });
  delete user._doc.password;
  response.sendResponse(res, user, 200);
};

exports.signin = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.errors[0].msg);
    return response.sendResponse(res, errors.errors[0].msg, 400);
  }
  const { email, password } = req.body;
  const user = await User.findOne({ email: email });
  if (!user)
    return response.sendResponse(
      res,
      "No User registered with this email",
      400
    );

  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    return response.sendResponse(res, "Invalid Credentials", 400);
  }
  delete user._doc.password;
  const token = await jwt.sign({ user: user }, credentails.jwtSecret, {
    expiresIn: credentails.jwtExpiry
  });

  res.cookie("token", token, { expire: new Date() + 9999 });

  return response.sendResponse(res, token, 200);
};

exports.signout = async (req, res) => {
  req.user = null;
  res.clearCookie("token");

  response.sendResponse(res, "User Signed Out Successfully", 200);
};
