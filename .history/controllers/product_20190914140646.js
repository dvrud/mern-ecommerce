const Product = require("../models/product");
const response = require("../HelperFunctions/response");
const formidable = require("formidable");
const _ = require("lodash");
const fs = require("fs");

exports.createProduct = async (req, res) => {
  console.log(req.files);
  let images = [],
    newProduct = {};
  if (req.files) {
    for (const image of req.files) {
      images.push(image.url);
    }
    newProduct = {
      name,
      price,
      quantity,
      description,
      photo: images,
      category
    };
  } else {
    newProduct = {
      name,
      price,
      quantity,
      description,
      category
    };
  }
};

// let form = new formidable.IncomingForm();
// form.keepExtensions = true;
// form.parse(req, async (err, fields, files) => {
//   if (err)
//     return response.sendResponse(res, "Image Could not be uploaded", 400);

//   let product = new Product(fields);

//   if (files.photo) {
//     product.photo.data = fs.readFileSync(files.photo.path);
//     product.photo.contentType = files.photo.type;
//   }

//   await product.save().catch(err => {
//     response.sendResponse(res, err, 400);
//   });

//   response.sendResponse(res, product, 200);
// });
