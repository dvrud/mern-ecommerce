const Product = require("../models/product");
const response = require("../HelperFunctions/response");
const formidable = require("formidable");
const _ = require("lodash");
const fs = require("fs");
const { validationResult } = require("express-validator");
const cloudinary = require("cloudinary");

// Cloudinary config
cloudinary.config({
  cloud_name: "dlymhxrrs",
  api_key: "178736849279487",
  api_secret: "zeOYujTELAaGSqhXjcRCQX3GSYU"
});

exports.createProduct = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.errors[0].msg);
    return response.sendResponse(res, errors.errors[0].msg, 400);
  }
  const { name, description, price, quantity, category } = req.body;
  let images = [],
    newProduct = {};
  if (req.files) {
    for (const image of req.files) {
      images.push({ url: image.url, public_id: image.public_id });
    }
    newProduct = {
      name,
      price,
      quantity,
      description,
      photo: images,
      category
    };
  } else {
    newProduct = {
      name,
      price,
      quantity,
      description,
      category
    };
  }

  const product = await new Product(newProduct).save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, product, 200);
};

exports.singleProduct = async (req, res) => {
  const product = await Product.findOne({ _id: req.body.id }).catch(err => {
    return response.sendResponse(res, err, 400);
  });

  if (!product) {
    return response.sendResponse(res, "Product not found", 400);
  }
  response.sendResponse(res, product, 200);
};

exports.deleteProduct = async (req, res) => {
  if (req.user.user.role === 0) {
    return response.sendResponse(
      res,
      "You are not authorized to delete the product",
      400
    );
  }
  try {
    await Product.findByIdAndDelete(req.body.id);
    response.sendResponse(res, "Product Deleted Successfully", 200);
  } catch (error) {
    response.sendResponse(res, error, 400);
  }
};

exports.deleteImage = async (req, res) => {
  const result = await cloudinary.uploader
    .destroy(req.body.public_id)
    .catch(err => {
      return response.sendResponse(res, err, 400);
    });
  const product = await Product.findOne({ _id: req.body.id });

  if (result.result === "ok") {
    product.photo = await product.photo.filter(
      pic => pic.public_id !== req.body.public_id
    );

    await product.save();
  } else {
    return response.sendResponse(res, "Image not found", 400);
  }
  return response.sendResponse(res, product, 200);
};

exports.addImage = async (req, res) => {
  if (req.files) {
    const product = await Product.findOne({ _id: req.body.id });
    for (const image of req.files) {
      product.photo.push({ url: image.url, public_id: image.public_id });
    }
    await product.save();
    response.sendResponse(res, product, 200);
  } else {
    response.sendResponse(res, "Please Choose Valid Images", 400);
  }
};

exports.updateProduct = async (req, res) => {
  const { name, quantity, price, description, shipping } = req.body;
  const product = await Product.findOne({ _id: req.body.id });

  product.name = name;
  product.quantity = quantity;
  product.price = price;
  product.description = description;
  product.shipping = shipping;

  await product.save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  return response.sendResponse(res, product, 200);
};

exports.allProducts = async (req, res) => {
  const products = await Product.find().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, products, 200);
};

exports.list = async (req, res) => {
  let order = req.query.order ? req.query.order : "asc";
  let sortBy = req.query.sortBy ? req.query.sortBy : "_id";
  let limit = req.query.limit ? parseInt(req.query.limit) : 1;

  const products = await Product.find()
    .populate("category")
    .sort([[sortBy, order]])
    .limit(limit)
    .catch(err => {
      return response.sendResponse(res, err, 400);
    });

  return response.sendResponse(res, products, 200);
};

// Will find the product based on request product category and then from that all the product that has same category we will return
exports.relatedList = async (req, res) => {
  let limit = req.query.limit ? parseInt(req.query.limit) : 10;

  const product = await Product.findOne({ _id: req.params.productId }).catch(
    err => {
      return response.sendResponse(res, err, 400);
    }
  );

  const products = await Product.find({ category: product.category })
    .where("_id")
    .ne(product._id)
    .limit(limit)
    .populate("category", "_id name")
    .catch(err => {
      return response.sendResponse(res, err, 400);
    });

  return response.sendResponse(res, products, 200);
};

exports.listCategory = async (req, res) => {
  const products = await Product.distinct("category", {}).catch(err => {
    response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, products, 200);
};
