const Product = require("../models/product");
const response = require("../HelperFunctions/response");
const formidable = require("formidable");
const _ = require("lodash");
const fs = require("fs");

exports.createProduct = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.errors[0].msg);
    return response.sendResponse(res, errors.errors[0].msg, 400);
  }
  const { name, description, price, quantity, category } = req.body;
  let images = [],
    newProduct = {};
  if (req.files) {
    for (const image of req.files) {
      images.push({ url: image.url, public_id: image.public_id });
    }
    newProduct = {
      name,
      price,
      quantity,
      description,
      photo: images,
      category
    };
  } else {
    newProduct = {
      name,
      price,
      quantity,
      description,
      category
    };
  }

  const product = await new Product(newProduct).save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, product, 200);
};

// let form = new formidable.IncomingForm();
// form.keepExtensions = true;
// form.parse(req, async (err, fields, files) => {
//   if (err)
//     return response.sendResponse(res, "Image Could not be uploaded", 400);

//   let product = new Product(fields);

//   if (files.photo) {
//     product.photo.data = fs.readFileSync(files.photo.path);
//     product.photo.contentType = files.photo.type;
//   }

//   await product.save().catch(err => {
//     response.sendResponse(res, err, 400);
//   });

//   response.sendResponse(res, product, 200);
// });
