const Category = require("../models/category");
const response = require("../HelperFunctions/response");

exports.createCategory = async (req, res) => {
  console.log(req.user, "this is from category");
  const { name } = req.body;
  const newCategory = {
    name
  };

  const category = await new Category(newCategory).save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, category, 200);
};
