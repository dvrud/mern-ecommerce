const Category = require("../models/category");
const response = require("../HelperFunctions/response");
const Product = require("../models/product");
exports.createCategory = async (req, res) => {
  if (req.user.user.role === 0) {
    return response.sendResponse(
      res,
      "You are not authorized to perform this action .!",
      400
    );
  }
  const { name } = req.body;
  const newCategory = {
    name
  };

  const category = await new Category(newCategory).save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, category, 200);
};

exports.allCategories = async (req, res) => {
  const categories = await Category.find().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  return response.sendResponse(res, categories, 200);
};

exports.updateCategory = async (req, res) => {
  if (req.user.user.role === 0) {
    return response.sendResponse(
      res,
      "You are not authorized to perform this action",
      400
    );
  }
  const category = await Category.findOne({ _id: req.body.id });

  category.name = req.body.name;
  await category.save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, category, 200);
};

exports.deleteCategory = async (req, res) => {
  if (req.user.user.role === 0) {
    return response.sendResponse(
      res,
      "You are not authorized to perform this action",
      400
    );
  }
  const category = await Category.findByIdAndDelete(req.body.id).catch(err => {
    return response.sendResponse(res, err, 400);
  });

  const products = Product.find({ category: req.body.id }).catch(err => {
    return response.sendResponse(res, err, 400);
  });
  console.log(products);
  for (const product of products) {
    await product.findByIdAndDelete(product._id);
  }

  response.sendResponse(
    res,
    "Category and its product Successfully Deleted",
    200
  );
};
