const Category = require("../models/product");
const response = require("../HelperFunctions/response");

exports.createProduct = async (req, res) => {
  const { name, description, price, category, quantity, photo } = req.body;

  const newProduct = {
    name,
    description,
    price,
    category,
    quantity,
    photo
  };
};
