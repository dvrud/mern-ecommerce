const Product = require("../models/product");
const response = require("../HelperFunctions/response");
const formidable = require("formidable");
const _ = require("lodash");
const fs = require("fs");
const { validationResult } = require("express-validator");

exports.createProduct = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.errors[0].msg);
    return response.sendResponse(res, errors.errors[0].msg, 400);
  }
  const { name, description, price, quantity, category } = req.body;
  let images = [],
    newProduct = {};
  if (req.files) {
    for (const image of req.files) {
      images.push({ url: image.url, public_id: image.public_id });
    }
    newProduct = {
      name,
      price,
      quantity,
      description,
      photo: images,
      category
    };
  } else {
    newProduct = {
      name,
      price,
      quantity,
      description,
      category
    };
  }

  const product = await new Product(newProduct).save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, product, 200);
};

exports.singleProduct = async (req, res) => {
  const product = await Product.findOne({ _id: req.body.id }).catch(err => {
    return response.sendResponse(res, err, 400);
  });

  if (!product) {
    return response.sendResponse(res, "Product not found", 400);
  }
  response.sendResponse(res, product, 200);
};

exports.deleteProduct = async (req, res) => {
  if (req.user.user.role === 0) {
    return response.sendResponse(
      res,
      "You are not authorized to delete the product",
      400
    );
  }
  try {
    const product = await Product.findByIdAndDelete(req.body.id);
    response.sendResponse(res, "Product Deleted Successfully", 200);
  } catch (error) {
    response.sendResponse(res, error, 400);
  }
};
