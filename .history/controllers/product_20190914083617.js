const Category = require("../models/product");
const response = require("../HelperFunctions/response");
const formidable = require("formidable");
const _ = require("lodash");

exports.createProduct = async (req, res) => {
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err)
      return response.sendResponse(res, "Image Could not be uploaded", 400);
  });
  const { name, description, price, category, quantity, photo } = req.body;

  const newProduct = {
    name,
    description,
    price,
    category,
    quantity,
    photo
  };
};
