const User = require("../models/User");
const bcrypt = require("bcryptjs");
const response = require("../HelperFunctions/response");

exports.signup = async (req, res) => {
  const { name, password, email, about } = req.body;
  const newUser = {
    name,
    password,
    email,
    about
  };
  const users = await User.findOne({ email });
  if (users) {
    return response.sendResponse(res, "User Already Exist", 400);
  }

  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(password, salt);

  const user = await new User(newUser).save().catch(err => {
    response.sendResponse(res, err, 400);
  });
  delete user._doc.password;
  response.sendResponse(res, user, 200);
};
