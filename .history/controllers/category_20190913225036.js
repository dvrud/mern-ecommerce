const Category = require("../models/category");
const response = require("../HelperFunctions/response");

exports.createCategory = async (req, res) => {
  const category = new Category(req.body);

  await category.save().catch(err => {
    return response.sendResponse(res, err, 400);
  });

  response.sendResponse(res, category, 200);
};
