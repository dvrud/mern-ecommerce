const express = require("express");
require("dotenv").config();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const app = express();

// Database connection
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log(`Database connected on ${process.env.MONGO_URI}`);
  });

//   MOrgan middleware
app.use(morgan("dev"));
// Bodyparser middleware
bodyParser.json();
//   Importing the routes
app.use("/api", require("./routes/user"));
const port = process.env.PORT || 8001;

app.listen(port, () => {
  console.log(`Server Started on port ${port}`);
});
