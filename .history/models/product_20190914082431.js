const mongoose = require("mongoose");
const Schema, {ObjectId} = mongoose.Schema;

const productSchema = new Schema(
  {
    name: {
      type: String,
      trim: true
    },
    description: {
        type: String
    },
    price: {
        type: Number
    },
    category: {
        type: ObjectId,
        ref: 'category'
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("products", productSchema);
