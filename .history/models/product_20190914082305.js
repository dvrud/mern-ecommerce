const mongoose = require("mongoose");
const Schema, {ObjectId} = mongoose.Schema;

const productSchema = new Schema(
  {
    name: {
      type: String,
      trim: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("products", productSchema);
