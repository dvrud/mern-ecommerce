const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const crypto = require("crypto");
const uuid = require("uuid/v1");

const userSchema = new Schema({
  name: {
    type: String,
    trim: true,
    maxlength: 32
  },
  email: {
    type: String,
    trim: true,
    unique: true
  },
  password: {
    type: String
  },
  about: {
    type: String
  }
});
