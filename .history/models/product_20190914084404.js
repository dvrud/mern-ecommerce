const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { ObjectId } = mongoose.Schema;

const productSchema = new Schema(
  {
    name: {
      type: String,
      trim: true
    },
    description: {
      type: String
    },
    price: {
      type: Number
    },
    category: {
      type: ObjectId,
      ref: "category"
    },
    quantity: {
      type: Number
    },
    photo: {
      data: Buffer,
      contentType: String
    },
    shipping: {
      type: Boolean,
      default: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("products", productSchema);
