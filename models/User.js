const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: {
      type: String,
      trim: true,
      maxlength: 32
    },
    email: {
      type: String,
      trim: true,
      unique: true
    },
    password: {
      type: String
    },
    about: {
      type: String
    },
    role: {
      type: Number,
      default: 0
    },
    history: {
      type: Array,
      default: []
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("users", userSchema);

// // To create the virtual fields
// userSchema
//   .virtual("password")
//   .set(password => {
//     this._password = password;
//     this.salt = uuidv1();
//     this.hashed_password = this.encryptPassword(password);
//   })
//   .get(() => {
//     return this._password;
//   });

// userSchema.methods = {
//   encryptPassword: password => {
//     if (!password) return;
//     try {
//       return crypto
//         .createHmac("sha1", this.salt)
//         .update(password)
//         .digest("hex");
//     } catch (error) {
//       return;
//     }
//   }
// };
