const express = require("express");
const { check, validationResult } = require("express-validator");
const auth = require("../HelperFunctions/auth");
const {
  createCategory,
  allCategories,
  updateCategory,
  deleteCategory
} = require("../controllers/category");
const router = express.Router();

router.post("/createCategory", auth, createCategory);

router.get("/allCategories", auth, allCategories);

router.post("/updateCategory", auth, updateCategory);

router.delete("/deleteCategory", auth, deleteCategory);
module.exports = router;
